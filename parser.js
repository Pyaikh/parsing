const axios = require('axios');
const cheerio = require('cheerio');
const ExcelJS = require('exceljs');
const fs = require('fs');


const agencyData = 'https://ratingruneta.ru/agency-agima/';
const agimaURL = 'https://www.agima.ru/';


// Функция для парсинга данных с сайта
async function parseWebsiteData(url) {
    try {
        const response = await axios.get(url);
        const $ = cheerio.load(response.data);

        // Селекторы для извлечения данных
        const AgencyName = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > header > div > div._1TOc-3sUvU > div._3o0X7Gf6aq > h1').text();
        const WebSite = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > header > div > div._1TOc-3sUvU > div._3o0X7Gf6aq > a > span').text();
        const Description = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H.tdVP8n76uJ > div:nth-child(1) > div > div > article > div').text();
        const Experience = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > header > div > div._1TOc-3sUvU > div.Vovp9jF5WV > div.x42tMOLpwd > div > div:nth-child(1) > span').text();
        const Staff = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > header > div > div._1TOc-3sUvU > div.Vovp9jF5WV > div.x42tMOLpwd > div > div:nth-child(2) > span').text();
        const TopPlaces = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H.tdVP8n76uJ > div:nth-child(1) > div > div > article > ul').text();
        const TopManagementFirst = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H.tdVP8n76uJ > div:nth-child(2) > div._2vaOgfb_9p > div > div > div._11NPZsSSQa').text();
        const PositionFirst = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H.tdVP8n76uJ > div:nth-child(2) > div._2vaOgfb_9p > div > div > div._6HCeCzjBV-').text();
        const TopManagementSecond = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H.tdVP8n76uJ > div:nth-child(2) > div._2vaOgfb_9p > div > div:nth-child(2) > div._11NPZsSSQa').text();
        const PositionSecond = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H.tdVP8n76uJ > div:nth-child(2) > div._2vaOgfb_9p > div > div:nth-child(2) > div._6HCeCzjBV-').text();
        const VK = $('a._1OOlgax2iF.vkontakte').attr('href');
        const Telegram = $('a._1OOlgax2iF.telegram').attr('href');

        // Получение номера телефона если с стандартой странички

        async function getPhoneNumberFromRatingRuneta() {
            try {
                const response = await axios.get(agencyData);
                const $ = cheerio.load(response.data);
                const phoneNumber = $('#root > div > div.B3nxUSyZyS > div.I2o3OLE9-P._2HTx5VoHdp > div:nth-child(1) > div > div > div._3NK5zA5s-H._1Jk9CU5VKB > div._1UFdAVGY06 > section > div > div > div._2klfIkVNWf > div > div.gIu1K_DiyF > a').text();
                return phoneNumber;
            } catch (error) {
                console.error('Ошибка при получении номера телефона с ratingruneta.ru:', error);
                return null;
            }
        }

        // Если нету номера на основной попытка получить на офф. сайте
        async function getPhoneNumberFromAgima() {
            try {
                const response = await axios.get(agimaURL);
                const $ = cheerio.load(response.data);
                const phoneNumber = $('a.header__phone, a.header-menu__phone').text();
                return phoneNumber;
            } catch (error) {
                console.error('Ошибка при получении номера телефона с agima.ru:', error);
                return null;
            }
        }

        let phoneNumber;
        const ratingRunetaPhoneNumber = await getPhoneNumberFromRatingRuneta();
        const agimaPhoneNumber = await getPhoneNumberFromAgima();

        if (ratingRunetaPhoneNumber) {
            phoneNumber = ratingRunetaPhoneNumber;
        } else if (agimaPhoneNumber) {
            phoneNumber = agimaPhoneNumber;
        } else {
            phoneNumber = 'Номер не найден';
        }


        return {
            AgencyName,
            WebSite,
            Description,
            Experience,
            Staff,
            TopPlaces,
            TopManagementFirst,
            PositionFirst,
            TopManagementSecond,
            PositionSecond,
            //DevelopmentSite,
            //DevelopmentProg,
            PhoneNumber: phoneNumber,
            VK,
            Telegram,
        };
    } catch (error) {
        console.error('Ошибка при парсинге данных:', error);
        return null;
    }
}

// Функция для обновления таблицы Excel
async function updateExcelTable(data) {
    const fileName = 'data.xlsx';

    const workbook = new ExcelJS.Workbook();
    let worksheet;

    // Если файл Excel уже существует
    if (fs.existsSync(fileName)) {
        await workbook.xlsx.readFile(fileName);
        worksheet = workbook.getWorksheet(1);
    } else {
        // Если файла Excel нет
        worksheet = workbook.addWorksheet('Данные');
        const headings = Object.keys(data);
        worksheet.addRow(headings);
    }
    worksheet.addRow(Object.values(data));
    await workbook.xlsx.writeFile(fileName);
}

// URL Сайта
const url = agencyData;

// Обновление таблицы Excel
parseWebsiteData(url)
    .then((data) => {
        if (data) {
            return updateExcelTable(data);
        }
    })
    .then(() => {
        console.log('Данные успешно обновлены в таблице Excel.');
    })
    .catch((error) => {
        console.error('Произошла ошибка:', error);
    });
